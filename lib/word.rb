class Word

  attr_accessor :word, :level, :word_from

  #initialization
  def initialize(word,level,word_from = nil)
    @word = word
    @word_from = word_from
    @level = level
  end

  #to get the words that have only one charactor difference
  def get_next_level_words input_array
    words_to_remove_array = find_words_to_remove
    input_array = input_array - words_to_remove_array
    next_word_array = []
    len_word = @word.length
    input_array.each do |word|
      char_difference = 0
      for i in 0...len_word
        if word[i] != @word[i]
          char_difference += 1
        end

        if char_difference >= 2
          break
        end
      end
      if char_difference == 1
        next_word_array.push(word)
      end
    end
    return next_word_array
  end

  #remove the words that are already considered
  def find_words_to_remove
    words_array = []
    current_string = self
    while !current_string.word_from.nil?
      words_array<< current_string.word
      current_string = current_string.word_from
    end
    words_array
  end
end