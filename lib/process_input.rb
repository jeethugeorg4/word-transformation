class ProcessInput
  attr_accessor :start_word, :end_word, :approved_words

  #initialization
  def initialize(start_word,end_word,approved_words)
    @start_word = start_word
    @end_word = end_word
    @approved_words = approved_words
  end
  #process the input array
  def process_word
    level = 1
    start_array = []
    start_array << Word.new(@start_word,level)
    while !start_array.empty?
      tmp_word_obj = start_array.shift
      tmp_word_obj.get_next_level_words(@approved_words).each do |word|
        if word == @end_word
          return tmp_word_obj.level.to_i + 1
        end
        start_array << Word.new(word,tmp_word_obj.level.to_i + 1,tmp_word_obj)
      end
    end
    return "No match found"
  end
end

