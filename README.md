= Introduction =

This is a Ruby script that implements word transformation problem

= Requirements =

This code has been run and tested on Ruby 2.3 and 2.2

= Installation =

There is no specific gems used for this application. Gemfile not included 


= Run =

Run this application using ruby word_transformer.rb

user can give input strings for start word, end word and approved words

eg:

Enter Start Word:
hit

Enter End Word:
cog

Enter approved Words(Comma Seperated):
hot, dot, dog, lot, log,cog
