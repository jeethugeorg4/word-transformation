require_relative "lib/word"
require_relative "lib/process_input"

puts "\nEnter Start Word:"
start_word = gets.chomp
puts "\nEnter End Word:"
end_word = gets.chomp
puts "\nEnter approved Words(Comma Seperated):"
approved_words = gets.chomp
approved_words_array = approved_words.split(",").map(&:strip)
result = case
           when !(start_word.length).between?(2,5)
             "Argument error: start word size must be between 2 and 5"
           when start_word.size != end_word.size
             "Argument error: start word and end word not matching"
           when  !(approved_words_array.size.between?(2,600))
             "Argument error: input words count should be between 2 and 600"
           else
             ProcessInput.new(start_word,end_word,approved_words_array).process_word
         end

puts "The output is :   #{result}"



